﻿#pragma warning disable
using UnityEngine;

namespace NSScore
{
    public class InstallerXMLAulaToScoreController : MonoBehaviour
    {
        #region members

        [SerializeField] private XMLAulaController refXMLAulaController;

        [SerializeField] private ScoreController refScoreController;
        #endregion

        #region MonoBehaviour

        private void Start()
        {
            refScoreController.XMLDocumentAula = refXMLAulaController.XMLDocumentAula;
        }
        #endregion
    }
}