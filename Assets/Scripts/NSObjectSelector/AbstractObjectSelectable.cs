﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSObjectSelector
{
    [RequireComponent(typeof(BoxCollider))]
	public abstract class AbstractObjectSelectable : MonoBehaviour, IObjectSelectable
	{

        #region members

        protected bool isSelected;

 		#endregion

 		#region accesors

        public bool _isSelected
        {
            set
            {
                isSelected = value;
            }
        }

        #endregion

        #region public methods

        public virtual void SetSelected(bool argSelected)
        {
            isSelected = argSelected;
        }

        public bool GetIsDraggable()
        {
            return true;
        }

        public virtual void SetDraggedPosition(Vector3 argPosition, Vector3 argOffset)
        {
            transform.position = argPosition + argOffset;
        }

        #endregion

    }

}