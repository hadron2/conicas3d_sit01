﻿using NSGraficadoFuncionesConicas.FuncionAleatoria;
using NSInterfazAvanzada;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using TMPro;
using NSTraduccionIdiomas;

namespace NSRegistroDatos
{
    public class ControladorRegistroDatos : MonoBehaviour
    {
        #region members

        /// <summary>
        /// Panel en donde se selecciona la situacion, se usa saber que situacion fue calculada y traer sus datos.
        /// </summary>
        [SerializeField]
        private PanelInterfazBotonesSeleccionSituacion refPanelInterfazBotonesSeleccionSituacion;

        /// <summary>
        /// velocidad que sele da al usuario para resolver el ejercicio 
        /// </summary>
        [SerializeField]
        private float velocidad;

        public string TextoELipseSituacion;

        /// <summary>
        /// text que esta en elipse situasion
        /// </summary>
        public  TextMeshProUGUI  textElipse;
        [SerializeField]
        private DiccionarioIdiomas Dic;
      

        #endregion

        #region accesors

        #endregion

        #region monoBehaviour

        void Awake()
        {
           // Dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }

        // Use this for initialization
        void Start()
        {
          
            GenerarVelocidadVelocidad();
        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion

        #region private methods

        /// <summary>
        /// Consigue si el valor asignado se diferencia en mas de 10% del valor esperado
        /// </summary>
        /// <param name="argValorEsperado">Valor esperado</param>
        /// <param name="argValorAsignado">Valor asignado</param>
        /// <returns>Verdadero si el valor asignado tiene una diferencia de mas del 10% </returns>
        private bool GetErrorValor(float argValorEsperado, float argValorAsignado)
        {
            return Mathf.Abs(argValorEsperado - argValorAsignado) >= 0.025f;
        }
        #endregion
        private bool GetErrorValor2(float argValorEsperado, float argValorAsignado)
        {
            return Mathf.Abs(argValorEsperado - argValorAsignado) >= 0.06f;
        }
        #region public methods

        /// <summary>
        /// genera una nueva velocidad y la actualiza en eltexto de situacion 
        /// </summary>
        public void GenerarVelocidadVelocidad()
        {
            //TextoSituacionElipse
            velocidad =Random.Range(1f,5f);
           // Debug.Log(velocidad);
            textElipse.text = Dic.Traducir("TextoSituacionElipse", "") + " "+ velocidad.ToString("#.#", CultureInfo.InvariantCulture) + Dic.Traducir("UnidadesHoras", " unidades/hora");

        }


        /// <summary>
        /// Verifica si los datos introducidos para la recta en la ventana de registro son correctos
        /// </summary>
        /// <param name="argDatosRegistroRecta">Clase que contiene los puntos 1 y 2 que deben ser evaluados para saber si estan sobre la recta</param>
        /// <returns>Indice 0 el punto 1 esta sobre la trayectoria?, Indice 1 el punto 1 esta sobre la trayectoria?</returns>
        public bool[] VerificarDatosRecta(DatosRegistroRecta argDatosRegistroRecta, DatosRectaSimulador argDatosRectaSimulador)
        {
            var tmpDatosIncorrectos = new bool[2];
            var tmpM = argDatosRectaSimulador.m;
            var tmpB = argDatosRectaSimulador.b;

            tmpDatosIncorrectos[0] = GetErrorValor(tmpM * argDatosRegistroRecta.puntoSobreTrayectoria1[0] + tmpB, argDatosRegistroRecta.puntoSobreTrayectoria1[1]);
            tmpDatosIncorrectos[1] = GetErrorValor(tmpM * argDatosRegistroRecta.puntoSobreTrayectoria2[0] + tmpB, argDatosRegistroRecta.puntoSobreTrayectoria2[1]);
            return tmpDatosIncorrectos;
        }

        /// <summary>
        /// Verifica si los datos introducidos para la parabola en la ventana de registro son correctos
        /// </summary>
        /// <param name="argDatosRegistroParabola">Clase que contiene los datos registrados por el usuario</param>
        /// <param name="argDatosParabolaSimulador">Clase que contiene los datos de la formula original creada por el simulador</param>
        /// <returns>Indice 0 el punto esta sobre la trayectoria?, indice 1 la direccion de la parabola es correcta?</returns>
        public bool[] VerificarDatosParabola(DatosRegistroParabola argDatosRegistroParabola, DatosParabolaSimulador argDatosParabolaSimulador)
        {
            var tmpDatosIncorrectos = new bool[2];

            tmpDatosIncorrectos[0] = argDatosRegistroParabola.direccionParabola != argDatosParabolaSimulador.direccionParabola;//direccion de la parabola correcta

            switch (argDatosParabolaSimulador.direccionParabola)
            {
                ///vertical
                case DireccionParabola.abajo:
                case DireccionParabola.arriba:
                    tmpDatosIncorrectos[1] = GetErrorValor(Mathf.Pow(argDatosRegistroParabola.puntoSobreTrayectoria[0] - argDatosParabolaSimulador.h, 2) / (4 * argDatosParabolaSimulador.p) + argDatosParabolaSimulador.k, argDatosRegistroParabola.puntoSobreTrayectoria[1]);
                    break;

                ///horizontal
                case DireccionParabola.derecha:
                case DireccionParabola.izquierda:
                    tmpDatosIncorrectos[1] = GetErrorValor(Mathf.Pow(argDatosRegistroParabola.puntoSobreTrayectoria[1] - argDatosParabolaSimulador.k, 2) / (4 * argDatosParabolaSimulador.p) + argDatosParabolaSimulador.h, argDatosRegistroParabola.puntoSobreTrayectoria[0]);
                    break;
            }

            return tmpDatosIncorrectos;
        }

        /// <summary>
        /// Verifica si los datos introducidos para la circunferencia en la ventana de registro son correctos
        /// </summary>
        /// <param name="argDatosRegistroCircunferencia">Clase que contiene los datos registrados por el usuario</param>
        /// <param name="argDatosCircunferenciaSimulador">Clase que contiene los datos de la formula original creada por el simulador</param>
        /// <returns>Indice 0 el diametro es correcto?, indice 1 el perimetro es correcto?</returns>
        public bool[] VerificarDatosCircunferencia(DatosRegistroCircunferencia argDatosRegistroCircunferencia, DatosCircunferenciaSimulador argDatosCircunferenciaSimulador)
        {
            var tmpDatosIncorrectos = new bool[2];
            tmpDatosIncorrectos[0] = GetErrorValor(argDatosCircunferenciaSimulador.r * 2, argDatosRegistroCircunferencia.diametro);
            tmpDatosIncorrectos[1] = GetErrorValor2(2 * Mathf.PI * argDatosCircunferenciaSimulador.r , argDatosRegistroCircunferencia.perimetro);
            return tmpDatosIncorrectos;
        }

        /// <summary>
        /// Verifica si los datos introducidos para la elipse en la ventana de registro son correctos        
        /// </summary>
        /// <param name="argDatosRegistroElipse">Clase que contiene los datos registrados por el usuario</param>
        /// <param name="argDatosElipseSimulador">Clase que contiene los datos de la formula original creada por el simulador</param>
        /// <returns>Indice 0 la excentricidad es correcta?, indice 1 el tiempo de translacion es correcto?</returns>
        public bool[] VerificarDatosElipse(DatosRegistroElipse argDatosRegistroElipse, DatosElipseSimulador argDatosElipseSimulador)
        {
            var tmpDatosIncorrectos = new bool[2];
            //Verificar excentricidad
            float tmpExcentricidadCalculada;

            if (argDatosElipseSimulador.a > argDatosElipseSimulador.b)
            {
                var aux = Mathf.Sqrt((argDatosElipseSimulador.a * argDatosElipseSimulador.a) - (argDatosElipseSimulador.b * argDatosElipseSimulador.b)) / argDatosElipseSimulador.a;
                Debug.Log("excentricidad = " + aux);
            
                tmpExcentricidadCalculada = Mathf.Sqrt((argDatosElipseSimulador.a * argDatosElipseSimulador.a) - (argDatosElipseSimulador.b * argDatosElipseSimulador.b)) / argDatosElipseSimulador.a;

            }
            else
            {

                var aux= Mathf.Sqrt((argDatosElipseSimulador.b * argDatosElipseSimulador.b) - (argDatosElipseSimulador.a * argDatosElipseSimulador.a)) / argDatosElipseSimulador.b;
                Debug.Log("excentricidad = " + aux);

                tmpExcentricidadCalculada = Mathf.Sqrt((argDatosElipseSimulador.b * argDatosElipseSimulador.b) - (argDatosElipseSimulador.a * argDatosElipseSimulador.a)) / argDatosElipseSimulador.b;
            }

            tmpDatosIncorrectos[0] = GetErrorValor(tmpExcentricidadCalculada, argDatosRegistroElipse.excentricidad*100);
            Debug.Log("resultado retornado por al fusion error " + tmpDatosIncorrectos[0]);
            Debug.Log("excentricidad valor ex = " + argDatosRegistroElipse.excentricidad);


            //Verificar tiempo de translacion
            var tmpPerimetro = Mathf.PI * (3 * (argDatosElipseSimulador.a + argDatosElipseSimulador.b) - Mathf.Sqrt((3 * argDatosElipseSimulador.a + argDatosElipseSimulador.b) * (argDatosElipseSimulador.a + 3 * argDatosElipseSimulador.b)));
            var tmpTiempoTranslacion = tmpPerimetro / velocidad;// 4.2f;
            tmpDatosIncorrectos[1] = GetErrorValor(tmpTiempoTranslacion, argDatosRegistroElipse.tiempoTranslacion);
            return tmpDatosIncorrectos;
        }

        /// <summary>
        /// Verifica si los datos introducidos para la hiperbola en la ventana de registro son correctos        
        /// </summary>
        /// <param name="argDatosRegistroHiperbola">Clase que contiene los datos registrados por el usuario</param>
        /// <param name="argDatosHiperbolaSimulador">Clase que contiene los datos de la formula original creada por el simulador</param>
        /// <returns>Indice 0 la excentricidad es correcta?, Indice 1 el sentido es correcto?</returns>
        public bool[] VerificarDatosHiperbola(DatosRegistroHiperbola argDatosRegistroHiperbola, DatosHiperbolaSimulador argDatosHiperbolaSimulador)
        {
            var tmpDatosIncorrectos = new bool[2];

            float tmpExcentricidadCalculada;

            //if (argDatosHiperbolaSimulador.a > argDatosHiperbolaSimulador.b)
                tmpExcentricidadCalculada = Mathf.Sqrt(argDatosHiperbolaSimulador.a * argDatosHiperbolaSimulador.a + argDatosHiperbolaSimulador.b * argDatosHiperbolaSimulador.b) / argDatosHiperbolaSimulador.a;
            //else
            //    tmpExcentricidadCalculada = Mathf.Sqrt(argDatosHiperbolaSimulador.b * argDatosHiperbolaSimulador.b + argDatosHiperbolaSimulador.a * argDatosHiperbolaSimulador.a) / argDatosHiperbolaSimulador.b;

            tmpDatosIncorrectos[0] = GetErrorValor2(tmpExcentricidadCalculada, argDatosRegistroHiperbola.excentricidad);
            Debug.Log("hiperbolaExcen"+ tmpExcentricidadCalculada);
            tmpDatosIncorrectos[1] = argDatosRegistroHiperbola.sentidoHiperbola != argDatosHiperbolaSimulador.sentidoHiperbola;//direccion parabola correcta            
            return tmpDatosIncorrectos;
        }


        #endregion

        #region courutines

        #endregion
    }

    /// <summary>
    /// Contiene los datos de la recta registrados por el usuario
    /// </summary>
    public class DatosRegistroRecta
    {
        public Vector2 puntoSobreTrayectoria1;

        public Vector2 puntoSobreTrayectoria2;
    }

    /// <summary>
    /// Contiene los datos de la parabola registrados por el usuario
    /// </summary>
    public class DatosRegistroParabola
    {
        public Vector2 puntoSobreTrayectoria;

        public DireccionParabola direccionParabola;
    }

    /// <summary>
    /// Contiene los datos de la circunferencia registrados por el usuario
    /// </summary>
    public class DatosRegistroCircunferencia
    {
        public float diametro;

        public float perimetro;

        public FormaCircunferencia formaCircunferencia;
    }

    /// <summary>
    /// Contiene los datos de la elipse registrados por el usuario
    /// </summary>
    public class DatosRegistroElipse
    {
        public float excentricidad;

        public float tiempoTranslacion;
    }

    /// <summary>
    /// Contiene los datos de la hiperbola registrados por el usuario
    /// </summary>
    public class DatosRegistroHiperbola
    {
        public float excentricidad;

        public SentidoHiperbola sentidoHiperbola;
    }


    /// <summary>
    /// Contiene los datos de la recta creada al azar por el simulador
    /// </summary>
    public class DatosRectaSimulador
    {
        public float b;

        public float m;
    }

    /// <summary>
    /// Contiene los datos de la parabola creada al azar por el simulador
    /// </summary>
    public class DatosParabolaSimulador
    {
        public float h;

        public float k;

        public float p;

        public DireccionParabola direccionParabola;
    }

    /// <summary>
    /// Contiene los datos de la circunferencia creada al azar por el simulador
    /// </summary>
    public class DatosCircunferenciaSimulador
    {
        public float h;

        public float k;

        public float r;

        public FormaCircunferencia formaCircunferencia;
    }

    /// <summary>
    /// Contiene los datos de la elipse creada al azar por el simulador
    /// </summary>
    public class DatosElipseSimulador
    {
        public float h;

        public float k;

        public float a;

        public float b;
    }

    /// <summary>
    /// Contiene los datos de la hiperbola creada al azar por el simulador
    /// </summary>
    public class DatosHiperbolaSimulador
    {
        public float h;

        public float k;

        public float a;

        public float b;

        public SentidoHiperbola sentidoHiperbola;
    }
}