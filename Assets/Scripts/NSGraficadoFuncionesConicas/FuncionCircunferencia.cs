﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionCircunferencia : AbstractDibujoFuncion
	{
        #region members
        private float h;
        private float k;
        private float r;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        [SerializeField]
        private GameObject prefabPlanetaFoco;

        [SerializeField]
        private float velocidadObjetoOrbita = 0.25f;
        #endregion

        #region accesors

        public float _h
        {
            set
            {
                h = value;
            }
        }

        public float _k
        {
            set
            {
                k = value;
            }
        }

        public float _r
        {
            set
            {
                r = value;
            }
        }

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpAnguloCircunferencia = 0; tmpAnguloCircunferencia <= 360; tmpAnguloCircunferencia += 1f)
            {
                var tmpPoint = new Vector2(h + Mathf.Cos(tmpAnguloCircunferencia * Mathf.Deg2Rad)*r, k + Mathf.Sin(tmpAnguloCircunferencia * Mathf.Deg2Rad)*r);
                tmpListaPosicionesGrafica.Add(tmpPoint);
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {            
            CrearLineRenderer("Circunferencia", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

            var tmpPlanetaFoco = Instantiate(prefabPlanetaFoco, transform).GetComponent<Transform>();
            tmpPlanetaFoco.localPosition = new Vector2(h, k);

            var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
            tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);
        }        
        #endregion

        #region courutines

        #endregion
    }
}