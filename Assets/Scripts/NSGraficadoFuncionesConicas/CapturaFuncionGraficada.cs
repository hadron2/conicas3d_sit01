﻿using NSCreacionPDF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    [RequireComponent(typeof(Camera))]
    public class CapturaFuncionGraficada : MonoBehaviour
    {
        #region members

        private Camera cameraCapturaGrafica;

        [SerializeField]
        private RenderTexture renderTextureGraficaFuncion;

        [SerializeField]
        private ControladorValoresPDF refControladorValoresPDF;

        #endregion

        #region public methods

        public void CapturarFuncionGraficada()
        {
            if (!cameraCapturaGrafica)
                cameraCapturaGrafica = GetComponent<Camera>();            
            
            cameraCapturaGrafica.gameObject.SetActive(true);
            StopAllCoroutines();
            StartCoroutine(CouCapturarImagePDF());
        }

        #endregion

        #region courutines

        private IEnumerator CouCapturarImagePDF()
        {
            yield return new WaitForEndOfFrame();
            cameraCapturaGrafica.Render();
            RenderTexture.active = renderTextureGraficaFuncion;
            var tmpTexture2D = new Texture2D(1080, 1080, TextureFormat.ARGB32, false, false);
            tmpTexture2D.ReadPixels(new Rect(0, 0, 1080, 1080), 0, 0);
            tmpTexture2D.Apply();            
            RenderTexture.active = null;
            refControladorValoresPDF.SetCapturaGraficaFuncion(tmpTexture2D);
            cameraCapturaGrafica.gameObject.SetActive(false);
        }
        #endregion
    }
}
