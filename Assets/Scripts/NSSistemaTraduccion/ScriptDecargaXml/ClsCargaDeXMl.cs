﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using NSTraduccionIdiomas;

public class ClsCargaDeXMl : MonoBehaviour {

    #region members
    public string[] NombreArchivoACargar;
    

    private Dictionary<string, string> DiccionarioConLoDelrAchivo;

    private DiccionarioIdiomas dic;
    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour
    private void Awake()
    {
        dic= this.GetComponent<DiccionarioIdiomas>();
    }
    private void Start()
    {
        DiccionarioConLoDelrAchivo = new Dictionary<string, string>();
        //mtdLeerXML(NombreArchivoACargar[0]);
    }

    #endregion

    #region private methods

    private void mtdLeerXML(string nombreDelArchivo) {

        DiccionarioConLoDelrAchivo = new Dictionary<string, string>();
        var dir=System.IO.Path.Combine( Application.streamingAssetsPath,nombreDelArchivo + ".xml");

        XmlDocument reader = new XmlDocument();
        reader.LoadXml(System.IO.File.ReadAllText(dir));


        XmlNodeList _list = reader.ChildNodes[0].ChildNodes;

        for (int i = 0; i < _list.Count; i++)
        {
            DiccionarioConLoDelrAchivo.Add(_list[i].Name, _list[i].InnerXml);
        }



    }




    #endregion

    #region public methods

    public Dictionary<string,string> CargarDiccionarioEspañol()
    {
        mtdLeerXML(NombreArchivoACargar[0]);
        return DiccionarioConLoDelrAchivo;
    }

    public Dictionary<string, string> CargarDiccionarioIngles()
    {
        mtdLeerXML(NombreArchivoACargar[1]);
        return DiccionarioConLoDelrAchivo;
    }

    public Dictionary<string, string> CargarDiccionarioPortugues()
    {
        mtdLeerXML(NombreArchivoACargar[1]);
        return DiccionarioConLoDelrAchivo;
    }

    public Dictionary<string, string> CargarDiccionarioTurco()
    {
        mtdLeerXML(NombreArchivoACargar[1]);
        return DiccionarioConLoDelrAchivo;
    }


    #endregion

    #region courutines

    #endregion


}
