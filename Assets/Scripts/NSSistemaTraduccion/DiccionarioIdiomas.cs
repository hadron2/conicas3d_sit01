﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.Networking;
using NSBoxMessage;

using System;
using System.IO;

namespace NSTraduccionIdiomas
{
    public class DiccionarioIdiomas : MonoBehaviour
    {
        #region members

        /// <summary>
        /// Diccionario para las traducciones al idioma ingles
        /// </summary>
        public Dictionary<string, string> diccionarioIngles = new Dictionary<string, string>();
        /// <summary>
        /// Diccionario para las traducciones al idioma portugues
        /// </summary>
        public Dictionary<string, string> diccionarioPortugues = new Dictionary<string, string>();
        /// <summary>
        /// Diccionario para las traducciones al idioma turco
        /// </summary>
        public Dictionary<string, string> diccionarioTurco = new Dictionary<string, string>();
        /// <summary>
        ///  Diccionario para las traducciones al idioma Español
        /// </summary>
        public Dictionary<string, string> diccionarioEspañol = new Dictionary<string, string>();
        /// <summary>
        /// contiene el nombre de los arhivos empezando por español seguido de ingles y asi dependeiendo del la cantidad de lenguajes
        /// </summary>
        public string[] NombreArchivoACargar;

        /// <summary>
        /// Idioma seleccionado actualmente
        /// </summary>
        private ListaIdiomas idiomaActual;
        //[SerializeField]
        //private BoxMessageManager refBoxMessageManager;

        #endregion

        #region delegates

        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce los objetos con el componente TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">Clase para obtener el texto para traducir</param>
        private delegate void delegateTraduccion(TextParaTraducir argTextParaTraducir);

        private delegateTraduccion DltTraduccion = delegate (TextParaTraducir argTextParaTraducir) { };

        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce un string
        /// </summary>
        /// <param name="argStringParaTraducir">String que se desea traducir</param>
        private delegate string delegateTraduccionString(string argNumbreTextParaTraducir, string texto);

        private delegateTraduccionString DltTraduccionString = delegate (string argNumbreTextParaTraducir, string texto) { return ""; };
       
        /// <summary>
        /// delegado al que se suscriben el metodo que traduce un text por su object name
        /// </summary>
        public delegate void delegateTraducirporNombreDelObjeto();

        public delegateTraducirporNombreDelObjeto DLTraducirForNameObj = delegate () { };
        #endregion

        #region monoBehaviour

        void Awake()
        {
            CargarDiccionarios();
           // SetIdioma(ListaIdiomas.espaniol);
        }
        private void Start()
        {
          // Debug.Log("loque esta en el diccionario  keyTituloSituacionRecta-> : " + diccionarioEspañol["TextoAyudaPracticaLibre"]);
        }
        #endregion

        #region private methods
        /// <summary>
        ///  metodo para traducir strings a español
        /// </summary>
        /// <param name="argStringParaTraducir">Nombre del objeto que tiene el text para traducir o llave</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionEspañol(string argNumbreTextParaTraducir,string texto)
        {
            if (diccionarioEspañol.ContainsKey(argNumbreTextParaTraducir))
            {
                return diccionarioEspañol[argNumbreTextParaTraducir];
            }
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        //Ingles
        /// <summary>
        /// Consigue la traduccion en ingles del texto de TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        private void GetTraduccionIngles(TextParaTraducir argTextParaTraducir)
        {
            if (diccionarioIngles.ContainsKey(argTextParaTraducir._textoOriginalEspaniol))
            {
                argTextParaTraducir.text = diccionarioIngles[argTextParaTraducir._textoOriginalEspaniol];
            }
            else
                TrackingTextoSinTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionIngles(string argNumbreTextParaTraducir, string texto)
        {
            if (diccionarioIngles.ContainsKey(argNumbreTextParaTraducir))
            {
                return diccionarioIngles[argNumbreTextParaTraducir];
            }
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        //turco
        /// <summary>
        /// Consigue la traduccion en ingles del texto de TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        private void GetTraduccionTurco(TextParaTraducir argTextParaTraducir)
        {
            if (diccionarioTurco.ContainsKey(argTextParaTraducir._textoOriginalEspaniol))
            {
                argTextParaTraducir.text = diccionarioTurco[argTextParaTraducir._textoOriginalEspaniol];
            }
            else
                TrackingTextoSinTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionTurco(string argNumbreTextParaTraducir, string texto)
        {
            if (diccionarioTurco.ContainsKey(argNumbreTextParaTraducir))
            {
                return diccionarioTurco[argNumbreTextParaTraducir];
            }
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        //portugues
        /// <summary>
        /// Consigue la traduccion en ingles del texto de TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        private void GetTraduccionPortugues(TextParaTraducir argTextParaTraducir)
        {
            if (diccionarioPortugues.ContainsKey(argTextParaTraducir._textoOriginalEspaniol))
            {
                argTextParaTraducir.text = diccionarioPortugues[argTextParaTraducir._textoOriginalEspaniol];
            }
            else
                TrackingTextoSinTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionPortugues(string argNumbreTextParaTraducir, string texto)
        {
            if (diccionarioPortugues.ContainsKey(argNumbreTextParaTraducir))
            {
                return diccionarioPortugues[argNumbreTextParaTraducir];
            }
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        /// <summary>
        /// Debug para encontrar facilmente el TextParaTraducir que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(TextParaTraducir argTextParaTraducir)
        {
            Transform tmpLastParent = argTextParaTraducir.transform;
            var tmpRutaObjetoSinTraduccion = "";

            while (tmpLastParent != null)
            {
                tmpRutaObjetoSinTraduccion += tmpLastParent.name + "/";
                tmpLastParent = tmpLastParent.parent;
            }

            Debug.LogWarning("Objeto sin traduccion en : " + tmpRutaObjetoSinTraduccion + " Con texto : " + argTextParaTraducir._textoOriginalEspaniol);
        }

        /// <summary>
        /// Debug para encontrar facilmente el string que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(string argStringParaTraducir)
        {
            Debug.LogWarning("String sin traduccion : " + argStringParaTraducir);
        }
        /// <summary>
        /// metodo que llena los diccionarios con las traducciones del xml
        /// </summary>
        ///
        private void CargarDiccionarios()
        {
           AsignarTraduccionesEspañol();
           AsignarTraduccionesIngles();
           AsignarTraduccionesPortugues();
           AsignarTraduccionesTurco();
        }


private IEnumerator mtdLeerXML(string NombreText,int id)
 {

           var dir = System.IO.Path.Combine(Application.streamingAssetsPath, NombreText + ".xml");
           XmlDocument reader = new XmlDocument();


#if UNITY_ANDROID
           /*try
            {
                var _dir = "/storage/emulated/0/";
                var _dir0 = "/storage/sdcard/";
                var _dir1 = "/storage/sdcard0/";
                var _dir2 = "/storage/sdcard1/";
                var _dirx = "/sdcard/";
                DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
                DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
                DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
                DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
                DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

                if (currentDirectory.Exists)
                {
                   dir= "jar:file://" + _dir + "!/assets/" + NombreText + ".xml"; 
                }
                else if (currentDirectory_0.Exists)
                {
                    dir = "jar:file://" + _dir0 + "!/assets/" + NombreText + ".xml";
                }
                else if (currentDirectory_1.Exists)
                {
                    dir = "jar:file://" + _dir1 + "!/assets/" + NombreText + ".xml";
                }
                else if (currentDirectory_2.Exists)
                {

                    dir = "jar:file://" + _dir2 + "!/assets/" + NombreText + ".xml";
                }
                else if (currentDirectory_x.Exists)
                {

                    dir = "jar:file://" + _dirx + "!/assets/" + NombreText + ".xml";
                }
            }
            catch (Exception error)
            {
                Debug.Log("no se encontro direccion viable");
            }*/


           dir = "jar:file://" + Application.dataPath + "!/assets/"+ NombreText + ".xml";
            WWW wwwfile = new WWW(dir);
            yield return wwwfile;

            if (!string.IsNullOrEmpty(wwwfile.error))
            {
                Debug.Log("no se encontro el archivo");
            }
            //refBoxMessageManager.MtdCreateBoxMessageInfo("conwebR"+wwwfile.text, "ACEPTAR");
            reader.LoadXml(wwwfile.text);


#elif UNITY_WEBGL
            UnityWebRequest www = UnityWebRequest.Get(dir);
            yield return www.SendWebRequest();
            reader.LoadXml( www.downloadHandler.text);

#else
            reader.LoadXml(System.IO.File.ReadAllText(dir));
            yield return null;
#endif

            XmlNodeList _list = reader.ChildNodes[0].ChildNodes;
            for (int i = 0; i < _list.Count; i++)
            {
               
                switch (id)
                {
                    case 0:
                        diccionarioEspañol.Add(_list[i].Name, _list[i].InnerXml);
                        break;
                    case 1:
                        diccionarioIngles.Add(_list[i].Name, _list[i].InnerXml);
                        break;
                    case 2:
                        diccionarioPortugues.Add(_list[i].Name, _list[i].InnerXml);
                        break;
                    case 3:
                        diccionarioTurco.Add(_list[i].Name, _list[i].InnerXml);
                        break;
                }
            }

}


#endregion
#region Traducciones Espeñol

        private void AsignarTraduccionesEspañol()
        {
           diccionarioEspañol.Clear();
            //diccionarioEspañol= gameObject.GetComponent<ClsCargaDeXMl>().CargarDiccionarioEspañol();
            StartCoroutine(mtdLeerXML(NombreArchivoACargar[0],0));
        }

#endregion

#region Traducciones ingles

            /// <summary>
            /// Agregar las traduccion para el idioma Ingles desde el español
            /// </summary>
        private void AsignarTraduccionesIngles()
        {
            diccionarioIngles.Clear();
            //diccionarioIngles= gameObject.GetComponent<ClsCargaDeXMl>().CargarDiccionarioIngles(); 
            StartCoroutine(mtdLeerXML(NombreArchivoACargar[1], 1));

        }

#endregion


#region Traducciones portugues 

        /// <summary>
        /// Agregar las traduccion para el idioma Portugues desde el español
        /// </summary>
        private void AsignarTraduccionesPortugues()
        {
            // diccionarioPortugues= gameObject.GetComponent<ClsCargaDeXMl>().CargarDiccionarioPortugues();
            StartCoroutine(mtdLeerXML(NombreArchivoACargar[2], 2));
        }

#endregion

#region Traducciones turco

            /// <summary>
            /// Agregar las traduccion para el idioma Turco desde el español
            /// </summary>
        private void AsignarTraduccionesTurco()
        {
            diccionarioTurco.Clear();
            //diccionarioTurco= gameObject.GetComponent<ClsCargaDeXMl>().CargarDiccionarioTurco(); 
            StartCoroutine(mtdLeerXML(NombreArchivoACargar[3], 3));

        }

#endregion

#region public methods

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        public void Traducir(TextParaTraducir argTextParaTraducir)
        {
            if (idiomaActual == ListaIdiomas.espaniol)
                return;
            
            DltTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string
        /// </summary>
        /// <param name="argTextParaTraducir">String para traducir</param>
        public string Traducir(string argNumbreTextParaTraducir, string texto)
        {
            /*if (idiomaActual == ListaIdiomas.espaniol)
                return argStringParaTraducir;*/

            return DltTraduccionString(argNumbreTextParaTraducir, texto);
        }

        /// <summary>
        /// Asigna un idioma
        /// </summary>
        /// <param name="argIdioma">Idioma que se usara</param>
        public void SetIdioma(ListaIdiomas argIdioma)
        {
            switch (argIdioma)
            {
                case ListaIdiomas.espaniol:

                    idiomaActual = argIdioma;
                    DltTraduccion =null;
                    DltTraduccionString = GetTraduccionEspañol;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.ingles:
                    idiomaActual = argIdioma;
                   //AsignarTraduccionesIngles();
                    DltTraduccion = GetTraduccionIngles;
                    DltTraduccionString = GetTraduccionIngles;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.portugues:
                    idiomaActual = argIdioma;
                   //AsignarTraduccionesPortugues();
                    DltTraduccion = GetTraduccionPortugues;
                    DltTraduccionString = GetTraduccionPortugues;
                    DLTraducirForNameObj();
                    break;
                case ListaIdiomas.turco:
                    idiomaActual = argIdioma;
                   //AsignarTraduccionesTurco();
                    DltTraduccion = GetTraduccionTurco;
                    DltTraduccionString = GetTraduccionTurco;
                    DLTraducirForNameObj();
                    break;
            }
        }

#endregion
    }

    public enum ListaIdiomas
    {
        espaniol,
        ingles,
        portugues,
        turco
    }

}