﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using NSInterfazAvanzada;
using NsSeguridad;

public class ConeccionAulaWeb : MonoBehaviour {

    [SerializeField] private PanelInterfazLogin2Campos Loguin2campos;
    [SerializeField] private ClsSeguridad seguridad;


    public void ResivirDatosApi()
    {

#if UNITY_WEBGL
        //CÓDIGO BASE_SITUATION
        //Url
        if (seguridad.ModoAula)
        {

            Debug.Log("entra Web aula");
            Loguin2campos.inputUsuario.enabled = false;
            Loguin2campos.inputPassword.enabled = false;

            string urlWebGL = seguridad.WebGLUrl;
            string webGLCode = seguridad.webGLCode;

            WWWForm form = new WWWForm();
            form.AddField("codigo", webGLCode);

            WWW wwwAulaWebGL = new WWW(urlWebGL, form);
            StartCoroutine(WaitForRequestAulaWebGL(wwwAulaWebGL));

        }
#endif
    
    }

    private IEnumerator WaitForRequestAulaWebGL(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            try
            {
                Debug.Log("lo que recibo en el www= "+www.text);
                var dataReceived = JSON.Parse(www.text);
                if (dataReceived["RESULT"] != null)
                {
                Loguin2campos.inputUsuario.text = dataReceived["RESULT"]["email"].Value;
                Loguin2campos.inputPassword.text = dataReceived["RESULT"]["password"].Value;
                }
            }
            catch (Exception error)
            {
                Debug.Log(error);
            }
        }
    }

}
