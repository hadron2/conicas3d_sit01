﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSegundaSala
{
    public class CalcularCortesEsferaPlano : MonoBehaviour
    {
        [SerializeField]
        private Transform esfera;

        [SerializeField]
        private Transform plano;

        [SerializeField]
        private Transform puntoCollider;

        private void FixedUpdate()
        {
            CacularPosicionPuntos();
        }

        private void CacularPosicionPuntos()
        {
            var tmpPlaneCollision = new Plane(plano.forward, plano.position);
            puntoCollider.position = tmpPlaneCollision.ClosestPointOnPlane(esfera.position);

            /*
            var tmpRayUp = new Ray(esfera.position, esfera.up * 0.5f * esfera.localScale.magnitude);
            var tmpRayDown = new Ray(esfera.position, -esfera.up * 0.5f * esfera.localScale.magnitude);
            var tmpRayDistanceHit = Mathf.Infinity;

            if (tmpPlaneCollision.Raycast(tmpRayUp, out tmpRayDistanceHit))
            {
                if (tmpRayDistanceHit <= 0.5f * esfera.localScale.magnitude)
                    puntoCollider.position = esfera.position + esfera.up * tmpRayDistanceHit;
                else
                    puntoCollider.position = tmpPlaneCollision.ClosestPointOnPlane(esfera.position + esfera.up * 0.5f * esfera.localScale.magnitude);
            }
            else if (tmpPlaneCollision.Raycast(tmpRayDown, out tmpRayDistanceHit))
            {
                if (tmpRayDistanceHit <= 0.5f * esfera.localScale.magnitude)
                    puntoCollider.position = esfera.position - esfera.up * tmpRayDistanceHit;
                else
                    puntoCollider.position = tmpPlaneCollision.ClosestPointOnPlane(esfera.position - esfera.up * 0.5f * esfera.localScale.magnitude);
            }
            else
            {
                var tmpClosestPointOnPlane = tmpPlaneCollision.ClosestPointOnPlane(esfera.position);
                puntoCollider.position = tmpClosestPointOnPlane;
            }*/
        }
    }
}