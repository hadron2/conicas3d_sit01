﻿using NSGraficadoFuncionesConicas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    [RequireComponent(typeof(LineRenderer))]
    public class DibujarCortesPlano : MonoBehaviour
    {
        [SerializeField]
        private LayerMask layerMaskFigura;

        [SerializeField]
        private LineRenderer refLineRenderActual;

        private Vector3 posicionActualFigura;

        private Quaternion rotacionActualFigura;

        private Vector3 escalaActualFigura;

        private Vector3 posicionActualPlano;

        private Quaternion rotacionActualPlano;

        [SerializeField]
        private Transform transformFigura;

        [SerializeField]
        private Transform transformPlano;

        private Vector3[] posicionesRutaXY;

        public Material MaterialLIneDeCorte;

        public Color colorGizmo;

        // Use this for initialization
        void Start()
        {
            CalcularCordenadasFuncionMatematica();
            posicionActualFigura = transformFigura.position;
            rotacionActualFigura = transformFigura.rotation;
            escalaActualFigura = transform.localScale;
            refLineRenderActual.widthMultiplier = 0.02f;
            refLineRenderActual.useWorldSpace = true;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            CalcularCordenadasFuncionMatematica();
        } 

        private void OnDrawGizmos()
        {
            Gizmos.color = colorGizmo;
            Gizmos.DrawCube(transform.position, Vector3.one * 0.2f);
            Gizmos.DrawRay(new Ray(transform.position, transform.forward*10));

            for (float tmpAnguloCircunferencia = 0; tmpAnguloCircunferencia <= 360; tmpAnguloCircunferencia += 20f)
            {
                var tmpPointDirection = transform.rotation * Quaternion.Euler(0, 0, tmpAnguloCircunferencia)*Vector3.up;
                var tmpRay = new Ray(transform.position, tmpPointDirection);
                Gizmos.DrawRay(tmpRay);
            }
        }

        public Vector3[] CalcularCordenadasFuncionMatematica()
        {
            if (posicionActualFigura != transformFigura.position 
                || rotacionActualFigura != transformFigura.rotation 
                || escalaActualFigura != transformFigura.localScale 
                || posicionActualPlano != transformPlano.position 
                || rotacionActualPlano != transformPlano.rotation)
            {
                posicionActualFigura = transformFigura.position;
                rotacionActualFigura = transformFigura.rotation;
                escalaActualFigura = transformFigura.localScale;
                posicionActualPlano = transformPlano.position;
                rotacionActualPlano = transformPlano.rotation;

                var tmpListaPosicionesGrafica = new List<Vector3>();
                transform.localRotation = Quaternion.identity;

                for (float tmpAnguloCircunferencia = 0; tmpAnguloCircunferencia <= 360; tmpAnguloCircunferencia += 1f)
                {
                    var tmpPointDirection = transform.rotation * Quaternion.Euler(0, 0, tmpAnguloCircunferencia) * Vector3.up;
                    var tmpRay = new Ray(transform.position, tmpPointDirection);

                    RaycastHit tmpRayHit;

                    if (Physics.Raycast(tmpRay, out tmpRayHit, Mathf.Infinity, layerMaskFigura))                    
                        tmpListaPosicionesGrafica.Add(tmpRayHit.point);                    
                }

                if (tmpListaPosicionesGrafica.Count > 0)
                    tmpListaPosicionesGrafica.Add(tmpListaPosicionesGrafica[0]);

                posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
                DibujarFuncion(MaterialLIneDeCorte, false);
                Debug.Log("CalcularCordenadasFuncionMatematica");
            }

            return posicionesRutaXY;
        }

        public void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {          
            refLineRenderActual.material = argMaterialDibujo;
            refLineRenderActual.positionCount = posicionesRutaXY.Length;
            refLineRenderActual.SetPositions(posicionesRutaXY);
        }


    }
}
