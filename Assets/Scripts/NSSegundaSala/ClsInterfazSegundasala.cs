﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{

    public class ClsInterfazSegundasala : AbstractPanelInterfaz
    {

        #region members

        private float cambio;
        private bool activar;
        private List<float> valoresINicialesSLiders;

        public Toggle btnEsfera;
        public Toggle btnCilindro;
        public Toggle btnCono;


        public GameObject ConosDoble;
        public GameObject Sphere;
        public GameObject Cilindro;
        public GameObject Plano;
        public Slider[] sliderInterfaz;

        private Vector3 posInicialConos;
        private Vector3 posInicialSphere;
        private Vector3 posInicialCilindro;
        private Vector3 posInicialPlano;
        private Quaternion RotacionInicia;
        private Transform figuraSeleccionada;
        private TipoAccion accion;




        //movimiento plano
        private Vector3 posicionPlanoNueva;
        private Vector3 rotacionPlanoNueva;

        //movimiento figura
        private Vector3 posicionFiguraNueva;

        #endregion

        #region enum

        public enum TipoAccion
        {
            moverX,
            moverY,
            moverZ,
            rotarY,
            rotarX,
            escalar
        }

        #endregion


        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        // Use this for initialization
        void Start()
        {
            valoresINicialesSLiders = new List<float>();
            llenarListaValoresIniciales();
            posInicialConos = ConosDoble.transform.position;
            posInicialSphere = Sphere.transform.position;
            posInicialCilindro = Sphere.transform.position;
            posInicialPlano = Plano.transform.position;
            RotacionInicia = Plano.transform.rotation;
            mtdSeleccionarFigura(0);
            activar = false;
            MtdRotarPlanoEnX(45f);
         
        }

        // Update is called once per frame
        void Update()
        {
            if (activar)
            {
                EjecutarRotaciones();
            }
        }


        #endregion

        #region private methods


        private void EjecutarRotaciones()
        {
           
            Plano.transform.position = Vector3.Lerp(Plano.transform.position, posicionPlanoNueva, 0.5f);
            Plano.transform.rotation = Quaternion.Slerp(Plano.transform.rotation, Quaternion.Euler(rotacionPlanoNueva), 0.5f);

            figuraSeleccionada.position = Vector3.Lerp(figuraSeleccionada.position, posicionFiguraNueva, 0.5f);
        }

        private void llenarListaValoresIniciales()
        {
            for (int i = 0; i < sliderInterfaz.Length; i++)
            {
                valoresINicialesSLiders.Add(sliderInterfaz[i].value);
            }

        }

        private void ReIniciarsliders()
        {
            for (int i = 0; i < sliderInterfaz.Length; i++)
            {
                sliderInterfaz[i].value = valoresINicialesSLiders[i];
            }
        }

        #endregion

        #region public methods


        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
            if (!argActivar) {
                Cilindro.SetActive(argActivar);
                ConosDoble.SetActive(argActivar);
                Sphere.SetActive(argActivar);
            }

        }


        public void MtdMoverFiguraEnX(float argValorSlider)
        {
            posicionFiguraNueva[0] = argValorSlider;
        }


        public void MtdMoverFiguraEnZ(float argValorSlider)
        {
            posicionFiguraNueva[2] = argValorSlider;
        }


        public void MtdMoverPlanoEnZ(float argValorSlider)
        {
            posicionPlanoNueva[1] = argValorSlider;
        }

        public void MtdRotarPlanoEnX(float argValorSlider)
        {
            rotacionPlanoNueva[0] = argValorSlider;
        }

        public void MtdRotarPlanoEnZ(float argValorSlider)
        {   
            rotacionPlanoNueva[2] = argValorSlider;
        }


        public void MtdEscalarFigura(float argValorSlider)
        {
            activar = true;
            switch (figuraSeleccionada.name)
            {
                case "Esfera3D":
                    figuraSeleccionada.localScale = Vector3.one * argValorSlider;
                    break;

                case "Cono3D":
                    figuraSeleccionada.localScale = new Vector3(argValorSlider, 1, argValorSlider);
                    break;
                case "Cilindro3D":
                    figuraSeleccionada.localScale = new Vector3(argValorSlider, 1, argValorSlider);
                    break;
            }
        }


        public void mtdSeleccionarFigura(int figura)
        {
            activar = true;
            ReIniciarsliders();
            Plano.SetActive(false);
            Cilindro.SetActive(false);
            ConosDoble.SetActive(false);
            Sphere.SetActive(false);

            switch (figura)
            {
                case 0:
                    figuraSeleccionada = ConosDoble.transform;
                    Plano.SetActive(true);
                    ConosDoble.SetActive(true);

                    break;
                case 1:
                    figuraSeleccionada = Cilindro.transform;
                    Plano.SetActive(true);
                    Cilindro.SetActive(true);

                    break;
                case 2:

                    figuraSeleccionada = Sphere.transform;
                    Plano.SetActive(true);
                    Sphere.SetActive(true);

                    break;
            }

        }

        public void mtRestaurarValoresInciales()
        {
            mtdSeleccionarFigura(0);
            btnCilindro.isOn = false;
            btnCono.isOn = true;
            btnEsfera.isOn = false;
            activar = false;
            ReIniciarsliders();
            Plano.SetActive(false);
            Cilindro.SetActive(false);
            ConosDoble.SetActive(false);
            Sphere.SetActive(false);

        }



        #endregion

        #region courutines

        #endregion

    }


}