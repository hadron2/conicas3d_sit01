﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SimpleJSON;
using System.Text;
using UnityEngine;
using NsSeguridad;
using NSInterfazAvanzada;
using NSBoxMessage;
using NSTraduccionIdiomas;
using Crosstales.FB;



 
public class clsEnvioPdf : MonoBehaviour
{
        
    #region members

    public Texture2D[] prueba;///tenporarl

    /// <summary>
    /// codigo unico por cada situacion, define a que practica pertenese el reporte 
    /// </summary>
    public string lab_code_report;
    /// <summary>
    /// veces que el usuario repitio la practica actual
    /// </summary>
    public string intentos;
    /// <summary>
    /// como su nombre lo indica es el timpo transcurrido desde qeu empeso la practica hasta que terino 
    /// </summary>
    public string timpoQueDuroLaPrectica;
    /// <summary>
    /// panel de mensaje
    /// </summary>
    public PanelInterfazMensaje mensaje;
    /// <summary>
    /// calificacion obtenida en la practica actual
    /// </summary>
    public string calificacion;
    /// <summary>
    /// es el nombre de la practica con la extenion .pdf
    /// </summary>
    public string NombrePdf;

    public float ValorMayor;

    /// <summary>
    /// imagen  para crear pdf
    /// </summary>
    private iTextSharp.text.Image pic1;

    /// <summary>
    /// documento auxiliar para la creacion del  pdf
    /// </summary>
    private Document doc = new Document();
    /// <summary>
    /// variable que indica si se debe enviar el pdf o solo mostrarlo
    /// </summary>
    private bool noEsUltimaHoja = true;
    /// <summary>
    /// referencia a la clase seguridad 
    /// </summary>
    private ClsSeguridad seguridad;
    /// <summary>
    /// array con los datos de usuario para la sesion 
    /// </summary>
    private string[] sesion;
    /// <summary>
    /// direcion donde se ubica el archivo
    /// </summary>
    private string path;
    /// <summary>
    /// imagen del documento pdf
    /// </summary>
    private Texture2D tex;
    /// <summary>
    /// tiempo maximo de espera par ala creacion del  pdf
    /// </summary>
    private float maxTiempo = 3;
    private string imagenHoja1;
    private string imagenHoja2;
    private byte[] fileBytesAula;
    private string NombreUsLoguin;
    [SerializeField]
    private BoxMessageManager refBoxMessageManager;
    private DiccionarioIdiomas dicIdiomas;

    private bool MensajeActivo;

    #endregion

    #region Delegate
    public delegate void DelegatetMensaje(string mensage);
    /// <summary>
    ///  delegado el cual se llamda a la hora de reportar un mesaje importante para el usuario, se le da como parametro el mensaje a mostrar
    /// </summary>
    public DelegatetMensaje DltMensajeEnvioPdf = delegate (string mensage) { };

    public delegate void DelegateBotonterminarWebGl();
    /// <summary>
    /// delegado que es llamado una vez se crea el pdf en web para que el ususario pueda continuar, el delagado debe activar el boton
    /// </summary>
    public DelegateBotonterminarWebGl DltBotontTerminarPdf = delegate () { };

    #endregion

    #region monoBehaviour

    void Awake()
    {
        seguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
        dicIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
    }

    void Start()
    {
        sesion = seguridad.GetDatosSesion();
    }
    #endregion

    #region private methods

    /// <summary>
    /// abre el pdf con un visor o en su defecto lo descarga, solo pc
    /// </summary>
    private void AbrirPdf_StandAlone()
    {
        if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            string pdfRuta = "file:///" + path;
            pdfRuta = Uri.EscapeUriString(pdfRuta);
            Application.OpenURL(pdfRuta);
        }
    }

    /// <summary>
    /// crea los directorios para guardar el pdf en las diferentes plataformas
    /// </summary>
    /// <param name="fileName"></param>
    private void DefinirPath(string fileName)
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {

#if UNITY_ANDROID
            try
            {
                var _dir = "/storage/emulated/0/";
                var _dir0 = "/storage/sdcard/";
                var _dir1 = "/storage/sdcard0/";
                var _dir2 = "/storage/sdcard1/";
                var _dirx = "/sdcard/";
                DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
                DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
                DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
                DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
                DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

                if (currentDirectory.Exists)
                {
                    path =  _dir ;
                }
                else if (currentDirectory_0.Exists)
                {
                    path =  _dir0 ;
                }
                else if (currentDirectory_1.Exists)
                {
                    path = _dir1 ;
                }
                else if (currentDirectory_2.Exists)
                {

                    path = _dir2;
                }
                else if (currentDirectory_x.Exists)
                {

                    path = _dirx;
                }
            }
            catch (Exception error)
            {
                Debug.Log("no se encontro direccion viable");
            }

            if (!Directory.Exists(path + "CloudLabs"))
            {
                System.IO.Directory.CreateDirectory(path + "CloudLabs");
            }
            path = path + "CloudLabs/" + fileName + ".pdf";

#elif UNITY_IPHONE

            path = Application.persistentDataPath + "/" + fileName + ".pdf";
#endif
        }
        else
        {

#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            path= FileBrowser.SaveFile("Save File", string.Empty, fileName, "pdf");
#else
            path = Application.dataPath + "/../" + fileName + ".pdf";
#endif

        }
    }

    /// <summary>
    /// metodo que verifica la subscripcion del usuario a la practica a la cual se va a enviar el reporte 
    /// </summary>
    private void leerAulaScore()
    {
        string _data = "{\"user\":\"" + NombreUsLoguin + "\",\"labCode\":\"" + lab_code_report + "\"}";
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(_data);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        string _url = seguridad.url_aula + "/externals/get_lab?data=" + encodedText;
        Debug.Log(_url);
        Debug.Log("esto es lo que se envia antes del reporte= " + _data);
        WWW wwwAulaGet = new WWW(_url);
        StartCoroutine(WaitForRequestAulaGet(wwwAulaGet));

    }

    /// <summary>
    /// metodo que prepara el envio de pdf y activa la corrutina que hace este envio 
    /// </summary>
    private void set_aula_score()
    {
        string _url = seguridad.url_aula + "/externals/put_lab"; // externals/put_lab http://localhost:3000/externals/put_lab
        string _date = DateTime.Now.ToString("yyyy/MM/dd");
        Debug.Log("calificacion "+calificacion);
        float notaAula= System.Convert.ToSingle(calificacion);
        notaAula = notaAula * 100f;
        WWWForm form = new WWWForm();
        string _params = "{\"user\":\"" + NombreUsLoguin + "\",\"labCode\":\"" + lab_code_report + "\",\"attempts\":" + intentos + ",\"delivery_date\":\"" + _date + "\",\"delivery_time\":\"" + timpoQueDuroLaPrectica + "\",\"app_score\":" + notaAula.ToString() + "}"; //+"1.8"+ "}"; la plataforma solo me esta amitiendo nuemros como notas
        var pdfNOm = NombrePdf + ".pdf";
        form.AddField("data", _params);
        Debug.Log("nombre del pdf antes de enviar " +pdfNOm);
        Debug.Log("url a la que se enviar "+ _url);
        Debug.Log("json "+_params);
        form.AddBinaryData("report_file", fileBytesAula, pdfNOm , "application/pdf");
        WWW wwwAulaPut = new WWW(_url, form);
        StartCoroutine(WaitForRequestAulaPut(wwwAulaPut));
    }


    /// <summary>
    /// mentodo encargado de crar el archivo pdf
    /// </summary>
    /// <param name="hojasPdf"></param>
    private void CrearImagenDos(Texture2D[] hojasPdf)
    {

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
          

            if (seguridad.LtiActivo == true)
            {
                StartCoroutine(BajarImagenParaWebPlayer(hojasPdf));
               // float notaLti = System.Convert.ToSingle(calificacion) / ValorMayor;

                string _url = seguridad.url_score_lti + "?score=" + calificacion + "&lti_params=" + seguridad.Lti_datos;
                Debug.Log(_url);
                WWW wwwLTI = new WWW(_url);
                StartCoroutine(WaitForRequestAulaLTI(wwwLTI));
            }
            else
            {
                StartCoroutine(BajarImagenParaWebPlayer(hojasPdf));
            }

        }

#if !UNITY_WEBGL
        int ancho = Convert.ToInt32(hojasPdf[0].width);
        int alto = Convert.ToInt32(hojasPdf[0].height); 
        path = Application.persistentDataPath + "/" + NombrePdf + ".pdf";
        // pdf_name_aula = NombrePdf + ".pdf";
        DefinirPath(NombrePdf);

        var rectangulo = new Rectangle(ancho, alto);
        doc = new Document(rectangulo);
        PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
        doc.SetMargins(0, 0, 0, 0);
        doc.Open();


        for (int it = 0; it < hojasPdf.Length; it++)
        {
            if (it == hojasPdf.Length - 1) noEsUltimaHoja = false;

            byte[] bytesImg = hojasPdf[it].EncodeToPNG();

                if (noEsUltimaHoja)
                {
                    pic1 = Image.GetInstance(bytesImg);
                    doc.Add(pic1);
                }
                else
                {
                    pic1 = Image.GetInstance(bytesImg);
                    doc.Add(pic1);
                    doc.Close();
                    //AbrirPdf_StandAlone();

                    if (seguridad.ModoAula == true && seguridad.coneccionActiva == true)
                    {
                        Debug.Log("direccion del archivo = "+path);
                        FileStream stream = File.OpenRead(path);
                        fileBytesAula = new byte[stream.Length];
                        stream.Read(fileBytesAula, 0, fileBytesAula.Length);
                        stream.Close();
                        Debug.Log(fileBytesAula.Length);
                        Debug.Log(fileBytesAula);
                        Debug.Log("eviando reporte");
                        leerAulaScore();//-------------------------------------------------------------------------------
                    }

                }
        }
#endif
#if !UNITY_EDITOR
		Destroy (tex);
#endif
    }


    /// <summary>
    /// mentodo encargado de crar el archivo pdf para la practiva libre
    /// </summary>
    /// <param name="hojasPdf"></param>
    private void CrearImagenDosPracticaLibre(Texture2D[] hojasPdf)
    {

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            StartCoroutine(BajarImagenParaWebPlayer(hojasPdf));
        }

#if !UNITY_WEBGL
        int ancho = Convert.ToInt32(hojasPdf[0].width);
        int alto = Convert.ToInt32(hojasPdf[0].height);
        path = Application.persistentDataPath + "/" + NombrePdf + ".pdf";
        DefinirPath(NombrePdf);

        var rectangulo = new Rectangle(ancho, alto);
        doc = new Document(rectangulo);
        PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
        doc.SetMargins(0, 0, 0, 0);
        doc.Open();


        for (int it = 0; it < hojasPdf.Length; it++)
        {
            if (it == hojasPdf.Length - 1) noEsUltimaHoja = false;

            byte[] bytesImg = hojasPdf[it].EncodeToPNG();

            if (noEsUltimaHoja)
            {
                pic1 = Image.GetInstance(bytesImg);
                doc.Add(pic1);
            }
            else
            {
                pic1 = Image.GetInstance(bytesImg);
                doc.Add(pic1);
                doc.Close();
                AbrirPdf_StandAlone();
                VisualizarPDF();

            }
        }
#endif
#if !UNITY_EDITOR
		Destroy (tex);
#endif
    }








#endregion

#region public methods

    /// <summary>
    /// metodo creado solo para probar la clase 
    /// </summary>
    public void MtdgenerarPdfEnSayo()
    {
        NombrePdf = "Secciones_Cónicas";
        CrearImagenDos(prueba);
    }

    /// <summary>
    /// metodo que se llama desde la web, confirma la generacion del pdf
    /// </summary>
    public void mtdMandarReporteWebAula(string pdfParaEnvioAula)
    {
        Debug.LogError("In GetDataFromWeb");
        Debug.LogError(pdfParaEnvioAula.Length);

        Debug.LogError("Convert to bytes");
        fileBytesAula = Convert.FromBase64String(pdfParaEnvioAula);
        Debug.LogError(fileBytesAula.Length);

        leerAulaScore();

    }


    /// <summary>
    /// metodo utilizado para visualizar el pdf en ios y android unicamente
    /// </summary>
    public void VisualizarPDF()
    {

#if UNITY_ANDROID
        if (!MensajeActivo)
        {
            MensajeActivo = true;
            refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("MensajeCarpetaPdf", ""), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"), activarpanel);
        }

#endif
 /*       
#if UNITY_ANDROID
                print("path: " + path);
                Application.OpenURL(path);
#endif*/

#if UNITY_IPHONE
                EtceteraBinding.showWebPage ( path , true );
#endif

    }

    public void activarpanel()
    {
        MensajeActivo = false;
    }

    /// <summary>
    /// metodo para enviar el pdf utilizado solo en android y ios
    /// </summary>
    public void Enviar_PDF_Correo()
    {
#if UNITY_ANDROID
        EtceteraAndroid.showEmailComposer("", "Reporte", "Reporte", false, path);
#endif

#if UNITY_IPHONE
		EtceteraBinding.showMailComposerWithAttachment( path , "application/pdf" , "Reporte" , "direccion" ,"Reporte " , "Reporte " , false);
#endif
    }

    /// <summary>
    /// se llama para crear el Pdf de la practica libre
    /// </summary>
    /// <param name="CodigoDeLaActividad">codigo unico de cada actividad entregado por innovative </param>
    /// <param name="NumIntentos">intentos que el usuario a realizado</param>
    /// <param name="tiempo">timepo de duracion de la practica</param>
    /// <param name="calificacionEnLaActividad">calificacion de la practica</param>
    /// <param name="nombrePactricaPdf">nombre de la practica a la cual sele creara el pdf</param>
    /// <param name="documentos">array de txturas2D que contiene cada hoja del documento pdf</param>
    /// <returns></returns>
    public void mtdDescargarPdfPracticalibre(string CodigoDeLaActividad, string NumIntentos, string tiempo, string calificacionEnLaActividad, string nombrePracticaPdf, Texture2D[] documentos)
    {
        sesion = seguridad.GetDatosSesion();
        noEsUltimaHoja = true;
        lab_code_report = CodigoDeLaActividad;
        intentos = NumIntentos;
        timpoQueDuroLaPrectica = tiempo;
        calificacion = calificacionEnLaActividad;
        NombrePdf =  nombrePracticaPdf;
        CrearImagenDosPracticaLibre(documentos);
    }

    /// <summary>
    /// se llama para enviar o crear el pdf
    /// </summary>
    /// <param name="CodigoDeLaActividad">codigo unico de cada actividad entregado por innovative </param>
    /// <param name="NumIntentos">intentos que el usuario a realizado</param>
    /// <param name="tiempo">timepo de duracion de la practica</param>
    /// <param name="calificacionEnLaActividad">calificacion de la practica</param>
    /// <param name="nombrePactricaPdf">nombre de la practica a la cual sele creara el pdf</param>
    /// <param name="documentos">array de txturas2D que contiene cada hoja del documento pdf</param>
    /// <returns></returns>
    public void mtdDescargaryEnviarReporte(string CodigoDeLaActividad, string NumIntentos, string tiempo, string calificacionEnLaActividad, string nombrePracticaPdf, Texture2D[] documentos)
    {

        NombreUsLoguin = seguridad.usuario;
        noEsUltimaHoja = true;
        lab_code_report = CodigoDeLaActividad;
        intentos = NumIntentos;
        timpoQueDuroLaPrectica = tiempo;
        calificacion = calificacionEnLaActividad;
        NombrePdf = dicIdiomas.Traducir("NombrePdf1","Documento");
        CrearImagenDos(documentos);

    }


#endregion

#region courutines

    /// <summary>
    /// corrutina que se encarga de descargar el pdf en webgl
    /// </summary>
    /// <param name="reporte"></param>
    /// <returns></returns>
    private IEnumerator BajarImagenParaWebPlayer(Texture2D[] reporte)
    {
        bool yaMellame=false;
        float tiempoTotal = 0;
        // int itAnt=-1;
        for (int it = 0; it < reporte.Length; it++)
        {
            if (it == reporte.Length - 1)
                noEsUltimaHoja = false;
            var jpgEncoder = new JPGEncoder(reporte[it], 100);

            while (!jpgEncoder.isDone && tiempoTotal < maxTiempo)
            {
                yield return null;
                tiempoTotal += Time.deltaTime;
            }

            if (jpgEncoder.isDone)
            {
                byte[] imgJPG = jpgEncoder.GetBytes();
                tiempoTotal = 0;

                if (noEsUltimaHoja)
                {
                    if (it == 0)
                    {
                        imagenHoja1 = "data:image/jpeg;base64," + Convert.ToBase64String(imgJPG);
                        Application.ExternalCall("pdfReporteHoja1", imagenHoja1, NombrePdf,seguridad.ModoAula);
                    }
                    else
                    {
                        imagenHoja1 = "data:image/jpeg;base64," + Convert.ToBase64String(imgJPG);
                        Application.ExternalCall("pdfReporteHojaINtermedia", imagenHoja1, NombrePdf);
                    }
                }
                else
                {
                    if (!yaMellame)
                    {
                        yaMellame = true;
                        imagenHoja2 = "data:image/jpeg;base64," + Convert.ToBase64String(imgJPG);
                        Application.ExternalCall("pdfReporteHojaFinal", imagenHoja2, NombrePdf);
                    }
                }

            }
        }
        yaMellame = false;

    }

    IEnumerator WaitForRequestAulaLTI(WWW www)
    {
        yield return www;
    }

    /// <summary>
    /// corrutina encargada de subir los datos al servidor asi como el archivo pdf
    /// </summary>
    /// <param name="www"></param>
    /// <returns></returns>
    private IEnumerator WaitForRequestAulaGet(WWW www)
    {

        yield return www;

        if (www.error == null)
        {
            byte[] decodedBytes = Convert.FromBase64String(www.text);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);
            Debug.Log(decodedText);
            var _data = JSON.Parse(decodedText);

            if (_data["state"] != null)
            {
                if (_data["state"].Value == "true")
                {
                    if (_data["res_code"] != null)
                    {
                        switch (_data["res_code"].Value)
                        {
                            case "USER_NOT_FOUND":
                                // DltMensajeEnvioPdf("El usuario no se encuentra registrado. Por favor comuníquese con su profesor.");//mensaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeUSER_NOT_FOUND", "El usuario no se encuentra registrado. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                //en diaccionario
                                break;
                            case "LAB_NOT_ASSIGNED":

                                //DltMensajeEnvioPdf("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");//mensaje conboton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_NOT_ASSIGNED", "El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                //en diccionario
                                break;
                            case "LAB_NOT_FOUND":

                                //DltMensajeEnvioPdf("No se encuentra la práctica. Por favor comuníquese con su profesor.");//mensaje conboton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_NOT_FOUND", "No se encuentra la práctica. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                break;
                            case "STATUS_OK":
                                if (_data["lab_state"].Value == "0")
                                {
                                    Debug.Log("OK");
                                    set_aula_score(); // se a validado 
                                }
                                else
                                {

                                    // DltMensajeEnvioPdf("Usted ya ha registrado una calificación y un reporte de laboratorio para esta práctica. Los datos generados no pudieron ser guardados. Por favor comuníquese con su profesor.");// mensaje con boton aceptar
                                    refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeSTATUS_OKconValorDiferenteDE0", "Usted ya ha registrado una calificación y un reporte de laboratorio para esta práctica. Los datos generados no pudieron ser guardados. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                }
                                break;
                            case "DB_EXCEPTION":

                                // DltMensajeEnvioPdf("Error en la base de datos. Por favor comuníquese con su proveedor.");// mensaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDB_EXCEPTION", "Error en la base de datos. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));

                                break;
                        }
                    }
                    else
                    {

                        // DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// panel con aceptar
                        refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                    }
                }
                else
                {
                    //DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// panel con acceptar
                    refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                }
            }
            else
            {
                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                // DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// panel con acceptar
            }

        }
        else
        {
            // DltMensajeEnvioPdf("No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio.");
            refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeNosePudoEstableserConexion", "No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }
      
    }

    /// <summary>
    /// coorrutina a la cual retorna el mensaje de estatus de la practiva subida, es decir si ya exite sino esta inscrito ect
    /// </summary>
    /// <param name="www"></param>
    /// <returns></returns>
    private IEnumerator WaitForRequestAulaPut(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            byte[] decodedBytes = Convert.FromBase64String(www.text);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);
            Debug.Log("respuesta que da al enviar el pdf"+decodedText);
            var _data = JSON.Parse(decodedText);
            if (_data["state"] != null)
            {
                if (_data["state"].Value == "true")
                {
                    if (_data["res_code"] != null)
                    {
                        switch (_data["res_code"].Value)
                        {
                            case "LAB_INSERTED":
                                Debug.Log("succesful insert");
                                //DltMensajeEnvioPdf("El reporte de laboratorio y los datos de esta práctica han sido enviados a su profesor.");//mensaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_INSERTED", "El reporte de laboratorio y los datos de esta práctica han sido enviados a su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));

                                break;
                            case "LAB_NOT_ASSIGNED":
                                //DltMensajeEnvioPdf("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");//mensaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_INSERTED", "El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));

                                break;
                            case "LAB_NOT_FOUND":
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_NOT_FOUND", "No se encuentra la práctica. Por favor comuníquese con su profesor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                //DltMensajeEnvioPdf("No se encuentra la práctica. Por favor comuníquese con su profesor.");//mensaje con boton aceptar

                                break;
                            case "LAB_UPDATED":
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLAB_UPDATED", "El reporte de laboratorio y los datos de esta práctica han sido actualizados y enviados a su profesor.."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                //DltMensajeEnvioPdf("El reporte de laboratorio y los datos de esta práctica han sido actualizados y enviados a su profesor.");//mensaje con boton aceptar

                                break;
                            case "DB_EXCEPTION":

                                //DltMensajeEnvioPdf("Error en la base de datos. Por favor comuníquese con su proveedor.");// menaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDB_EXCEPTION", "Error en la base de datos. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));

                                break;
                            case "LICENSE_EXPIRED":

                                //DltMensajeEnvioPdf("Hay un problema con la licencia del gestor de aula, por favor comuníquese con el proveedor.");//mensaje con boton aceptar
                                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeLICENSE_EXPIRED", "Hay un problema con la licencia del gestor de aula, por favor comuníquese con el proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                                break;
                        }
                    }
                    else
                    {                        Debug.Log("3---envio");
                        // DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// mensaje con boton aceprtar
                        refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                    }
                }
                else
                {
                    //DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// mensaje con boton aceptar
                    Debug.Log("2---envio");
                    refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                }
            }
            else
            {
                //DltMensajeEnvioPdf("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");// mensaje con boton aceptar
                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                Debug.Log("1---envio");
            }

        }
        else
        {
            //DltMensajeEnvioPdf("No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio.");//mensaje con boton aceptar
            refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeNosePudoEstableserConexion", "No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }

    }


#endregion
}
