﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSInterfazAvanzada
{
    [CreateAssetMenu(menuName = "Panel Interfaz/Animation Curve")]
	public class AnimationCurvePanelInterfaz : ScriptableObject
	{
        #region members
        /// <summary>
        /// Curva de animacion que usara el panel de interfaz para ejecutar sus animaciones
        /// </summary>
        [SerializeField]
        private AnimationCurve animationCurve;
        #endregion

        #region accesors

        public AnimationCurve _animationCurve
        {
            get
            {
                return animationCurve;
            }
        }

        #endregion
    }
}