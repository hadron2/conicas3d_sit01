﻿using NSEvaluacion;
using NsSeguridad;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;
using NSBoxMessage;
using System.Globalization;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase encargada de capturar en imagen todas las hojas del PDF
    /// </summary>
	public class CanvasReportePDFSegundaSala : MonoBehaviour
    {
        #region members
        /// <summary>
        /// Camara que hace la captura
        /// </summary>
        [SerializeField]
        private Camera cameraCapturaPDF;
        /// <summary>
        /// Render texture en donde quedan capturas las imagenes de cada hoja
        /// </summary>
        [SerializeField]
        private RenderTexture renderTexturePDF;
        /// <summary>
        /// Puntero a la courutina que captura las imagenes del PDF
        /// </summary>
        private IEnumerator couCapturarImagenPDF;
        /// <summary>
        /// Array de texturas que son las imagenes de las hojas capturadas
        /// </summary>
        private Texture2D[] capturaHojasPdf;
        /// <summary>
        /// referencia a la clase que gestiona el envio del PDF
        /// </summary>
        [SerializeField]
        private clsEnvioPdf refEnvioPdf;
        /// <summary>
        /// referencia a la clase que gestiona la seguridad, esta es para obtener algunos datos del usuario
        /// </summary>
        [SerializeField, Header("Datos adicionales"), Space(10)]
        private ClsSeguridad refClsSeguridad;
        /// <summary>
        /// referencia a la clase que asigna todos los valores al PDF, nombre de usuario, nombre simulador etc
        /// </summary>
        [SerializeField]
        private ControladorValoresPDFSegundaSala refControladorValoresPDFSegundaSala;
        /// <summary>
        /// referencia a la clase que contiene los tiempos y intentos del usuario en la situacion seleccionada
        /// </summary>
        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;
        /// <summary>
        /// string del tiempo que se demoro el usuario en la situacion
        /// </summary>
        private string tiempoSituacion;
        /// <summary>
        /// Codigo de la situacion desarrollada por el usuario
        /// </summary>
        private string codigoActividad;
        /// <summary>
        /// Nombre de la situacion que desarrollo el usuario
        /// </summary>
        private string nombreSituacion;
        private DiccionarioIdiomas dic;

        [SerializeField]
        private BoxMessageManager refBoxMessageManager;


        #endregion

        #region MonoBehaviour 
        public void Awake()
        {
            dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }
        private void Start()
        {

        }
        #endregion

        #region accesors

        public string _codigoSituacion
        {
            set
            {
                codigoActividad = value;
            }
        }

       /* public string _nombreSituacion
        {
            set
            {
                nombreSituacion = value;
            }
        }*/

        #endregion

        #region public methods


        public void GenerarReporte()
        {
            refBoxMessageManager.MtdCreateBoxMessageDecision(dic.Traducir("MensajeGenerarReportePracticaLibre", "¿Estás  seguro de generar el reporte?"),dic.Traducir("TextBotonAceptar", "ACEPTAR"), dic.Traducir("TextBotonCancelar","CANCELAR"), CapturarImagenesPDF, null);
        }


        /// <summary>
        /// Metodo que asigna todos los valores al PDF y ejecuta la courutina para capturar todas las hojas del PDF
        /// </summary>
        private void CapturarImagenesPDF()
        {
            if (couCapturarImagenPDF == null)
            {
                Debug.Log("CapturarImagenesPDF");


                //refControladorValoresPDFSegundaSala.SetFechaInicio(DateTime.Today.ToShortDateString());
                refControladorValoresPDFSegundaSala.SetFechaInicio(DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                refControladorValoresPDFSegundaSala.SetIntentos("N/A");
                refControladorValoresPDFSegundaSala.SetCalificacion("N/A");
                refControladorValoresPDFSegundaSala.SetSituacion( dic.Traducir("NombrePracticaPracticaLibre","Práctica libre"));

                var tmpDatosLogin = refClsSeguridad.GetDatosSesion();

                refControladorValoresPDFSegundaSala.SetCurso(tmpDatosLogin[1]);
                refControladorValoresPDFSegundaSala.SetIDCurso(tmpDatosLogin[2]);
                refControladorValoresPDFSegundaSala.SetInstitucion(tmpDatosLogin[3]);
                refControladorValoresPDFSegundaSala.SetUsuario(tmpDatosLogin[4]);
                refControladorValoresPDFSegundaSala.SetUnidad(dic.Traducir("NombreUniadPracticaLibre", "Corte secciones cónicas."));

                var tmpSegundos = Mathf.Floor(refControladorDatosSesion._tiempoSituacion % 60);
                var tmpMinutos = Mathf.Floor(refControladorDatosSesion._tiempoSituacion / 60);
                tiempoSituacion = ((tmpMinutos < 10f) ? "0" + tmpMinutos.ToString() : tmpMinutos.ToString()) + ":" + ((tmpSegundos < 10f) ? "0" + tmpSegundos.ToString() : tmpSegundos.ToString());

                refControladorValoresPDFSegundaSala.SetTiempoPractica(tiempoSituacion);

                couCapturarImagenPDF = CouCapturarImagePDF();
                StartCoroutine(couCapturarImagenPDF);
            }
            else
                Debug.Log("Captura PDF en proceso");
        }

        #endregion

        #region courutines

        /// <summary>
        /// Courutina que activa cada hoja del PDF para tomarle una captura
        /// </summary>
        private IEnumerator CouCapturarImagePDF()
        {
            capturaHojasPdf = new Texture2D[transform.childCount];
            cameraCapturaPDF.gameObject.SetActive(true);
            //yield return new WaitForSeconds(1);//para que los textos tengan tiempo de traducirse

            for (int i = 0; i < transform.childCount; i++)
            {
                var tmpGameObjectHojaPdfActual = transform.GetChild(i).gameObject;
                tmpGameObjectHojaPdfActual.SetActive(true);
                yield return new WaitForEndOfFrame();
                cameraCapturaPDF.Render();
                RenderTexture.active = renderTexturePDF;
                var tmpTexture2D = new Texture2D(824, 1080, TextureFormat.ARGB32, false, true);
                tmpTexture2D.ReadPixels(new Rect(548, 0, 824, 1080), 0, 0);
                tmpTexture2D.Apply();
                capturaHojasPdf[i] = tmpTexture2D;
                RenderTexture.active = null;
                tmpGameObjectHojaPdfActual.SetActive(false);
            }
            nombreSituacion = dic.Traducir("PdfPracLibre", "PracticaLibre");
            couCapturarImagenPDF = null;
            cameraCapturaPDF.gameObject.SetActive(false);
            //refEnvioPdf.mtdDescargaryEnviarReporte(codigoActividad, refControladorDatosSesion._cantidadIntentos.ToString(), tiempoSituacion, "N/A", nombreSituacion, capturaHojasPdf);
            refEnvioPdf.mtdDescargarPdfPracticalibre(codigoActividad, refControladorDatosSesion._cantidadIntentos.ToString(), tiempoSituacion, "N/A", nombreSituacion, capturaHojasPdf);
        }
        #endregion
    }
}